package sn.isi.m2gl.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A SituationCovid.
 */
@Entity
@Table(name = "situation_covid")
public class SituationCovid implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre_test")
    private Integer nombreTest;

    @Column(name = "nombre_cas_positif")
    private Integer nombreCasPositif;

    @Column(name = "nombre_cas_negatif")
    private Integer nombreCasNegatif;

    @Column(name = "nombre_deces")
    private Integer nombreDeces;

    @Column(name = "nombre_gueris")
    private Integer nombreGueris;

    @Column(name = "date")
    private LocalDate date;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNombreTest() {
        return nombreTest;
    }

    public SituationCovid nombreTest(Integer nombreTest) {
        this.nombreTest = nombreTest;
        return this;
    }

    public void setNombreTest(Integer nombreTest) {
        this.nombreTest = nombreTest;
    }

    public Integer getNombreCasPositif() {
        return nombreCasPositif;
    }

    public SituationCovid nombreCasPositif(Integer nombreCasPositif) {
        this.nombreCasPositif = nombreCasPositif;
        return this;
    }

    public void setNombreCasPositif(Integer nombreCasPositif) {
        this.nombreCasPositif = nombreCasPositif;
    }

    public Integer getNombreCasNegatif() {
        return nombreCasNegatif;
    }

    public SituationCovid nombreCasNegatif(Integer nombreCasNegatif) {
        this.nombreCasNegatif = nombreCasNegatif;
        return this;
    }

    public void setNombreCasNegatif(Integer nombreCasNegatif) {
        this.nombreCasNegatif = nombreCasNegatif;
    }

    public Integer getNombreDeces() {
        return nombreDeces;
    }

    public SituationCovid nombreDeces(Integer nombreDeces) {
        this.nombreDeces = nombreDeces;
        return this;
    }

    public void setNombreDeces(Integer nombreDeces) {
        this.nombreDeces = nombreDeces;
    }

    public Integer getNombreGueris() {
        return nombreGueris;
    }

    public SituationCovid nombreGueris(Integer nombreGueris) {
        this.nombreGueris = nombreGueris;
        return this;
    }

    public void setNombreGueris(Integer nombreGueris) {
        this.nombreGueris = nombreGueris;
    }

    public LocalDate getDate() {
        return date;
    }

    public SituationCovid date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SituationCovid)) {
            return false;
        }
        return id != null && id.equals(((SituationCovid) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SituationCovid{" +
            "id=" + getId() +
            ", nombreTest=" + getNombreTest() +
            ", nombreCasPositif=" + getNombreCasPositif() +
            ", nombreCasNegatif=" + getNombreCasNegatif() +
            ", nombreDeces=" + getNombreDeces() +
            ", nombreGueris=" + getNombreGueris() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
